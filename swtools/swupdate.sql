
-- reset frequency table
DELETE FROM aoki_db;

-- reset sequence
DELETE FROM sqlite_sequence where name='aoki_db';

-- insert new data ignoring some frequency data issues
INSERT INTO aoki_db (
	frequency,
	station,
	broadcast_time,
	days_of_operation,
	language,
	transmitter_power,
	transmitter_azimuth,
	transmitter_location,
	transmitter_adm,
	geocoordinates,
	remarks
)
SELECT
	frequency,
	station,
	broadcast_time,
	days_of_operation,
	language,
	transmitter_power,
	transmitter_azimuth,
	transmitter_location,
	transmitter_adm,
	geocoordinates,
	remarks
FROM aoki_temp
WHERE frequency <> '';

-- ** day crossover lookup gap issue fix **
-- add columns for [start_time], [end_time], and [diff_time]
-- if [diff_time] is negative, then change [end_time] by adding extra hours across to the next day
-- query on [start_time] and [end_time] but display [broadcast_time]

-- split broadcast_time into start and end and calculate diff
UPDATE aoki_db
SET start_time = substr(broadcast_time, 1, 4),
	end_time = substr(broadcast_time, 6, 9),
	diff_time = substr(broadcast_time, 6, 9) - substr(broadcast_time, 1, 4);

-- set a new end time where diff is smaller than zero
UPDATE aoki_db
SET end_time = ('2400' - start_time) + (end_time - '0000') + start_time
WHERE diff_time < 0;

-- update the schedule table (use replace or format on read)
UPDATE schedule SET schedule_name = '{}', schedule_season = '{}';

-- compact the database
VACUUM;