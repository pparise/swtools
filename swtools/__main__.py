# -*- coding: utf-8 -*-

"""Allow swlookup to be executable through `python -m swtools`."""

from __future__ import absolute_import

from .swlookup import main


if __name__ == "__main__":
    main()
